# Java Chalanges Okky Agung Wibisono

The API Gateway for Java Chalanges based on Spring Boot with generated Swagger API documentation.

## How To

Run application locally for development

```bash
./mvnw clean spring-boot:run
```

Build application

```bash
./mvnw clean package
```

Run the jar

```bash
java -jar target/okkyagungwibisono-0.0.1-SNAPSHOT.jar
```