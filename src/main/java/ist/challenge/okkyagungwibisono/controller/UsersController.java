package ist.challenge.okkyagungwibisono.controller;

import ist.challenge.okkyagungwibisono.dto.LoginDto;
import ist.challenge.okkyagungwibisono.entity.User;
import ist.challenge.okkyagungwibisono.repository.UserRepository;
import ist.challenge.okkyagungwibisono.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private final UsersService usersService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/getUser")
    public List<User> getUser(){
        return usersService.getUser();
    }

    @PutMapping("/editUser/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Long id,@RequestBody LoginDto loginDto) {

        if(userRepository.existsByUsername(loginDto.getUsername())){
            return new ResponseEntity<>("Username sudah terpakai!", HttpStatus.CONFLICT);
        }

        Optional<User> users = userRepository.findById(id);
        User user = users.get();

        if(user.getPassword().equals(loginDto.getPassword())){
            return new ResponseEntity<>("Password tidak boleh sama dengan password sebelumnya", HttpStatus.BAD_REQUEST);
        }

        user.setUsername(loginDto.getUsername());
        user.setPassword(loginDto.getPassword());

        userRepository.save(user);

        return new ResponseEntity<>("Sukses edit User", HttpStatus.CREATED);
    }

}
