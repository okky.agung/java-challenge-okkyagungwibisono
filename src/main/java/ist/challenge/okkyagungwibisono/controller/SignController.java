package ist.challenge.okkyagungwibisono.controller;

import ist.challenge.okkyagungwibisono.dto.LoginDto;
import ist.challenge.okkyagungwibisono.entity.User;
import ist.challenge.okkyagungwibisono.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
@RequestMapping("/auth")
public class SignController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/login")
    public ResponseEntity<String> authenticateUser(@RequestBody LoginDto loginDto){

        if(loginDto.getUsername() == null || loginDto.getPassword() == null){
            return new ResponseEntity<>("Username dan / atau password kosong", HttpStatus.BAD_REQUEST);
        }

        if(userRepository.checkUserPassword(loginDto.getUsername(),loginDto.getPassword()) == 0){
            return new ResponseEntity<>("", HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity<>("Sukses Login.", HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody LoginDto loginDto){

        if(userRepository.existsByUsername(loginDto.getUsername())){
            return new ResponseEntity<>("Username sudah terpakai!", HttpStatus.CONFLICT);
        }

        User user = new User();
        user.setUsername(loginDto.getUsername());
        user.setPassword(loginDto.getPassword());

        userRepository.save(user);

        return new ResponseEntity<>("Sukses membuat User", HttpStatus.CREATED);

    }
}