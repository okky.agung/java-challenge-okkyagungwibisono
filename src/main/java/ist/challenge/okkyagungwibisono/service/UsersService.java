package ist.challenge.okkyagungwibisono.service;

import ist.challenge.okkyagungwibisono.entity.User;
import ist.challenge.okkyagungwibisono.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UsersService {

    @Autowired
    UserRepository userRepository;

    public List<User> getUser() {

        List<User> userListResponse = userRepository.findAll();

        return userListResponse;
    }

}
