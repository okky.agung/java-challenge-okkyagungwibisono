package ist.challenge.okkyagungwibisono.repository;

import ist.challenge.okkyagungwibisono.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    @Query("SELECT count(u) FROM User u where username=:username and password=:password")
    Integer checkUserPassword(String username, String password);
}
