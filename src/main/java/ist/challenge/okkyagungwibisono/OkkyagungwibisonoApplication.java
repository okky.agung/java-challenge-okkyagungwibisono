package ist.challenge.okkyagungwibisono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class OkkyagungwibisonoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OkkyagungwibisonoApplication.class, args);
	}

}
