package ist.challenge.okkyagungwibisono.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

    @Entity
    @Getter
    @Setter
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @NonNull
        @Column(nullable = false, unique = true,length = 25)
        private String username;

        @NonNull
        @Column(nullable = false,length = 25)
        private String password;

    }